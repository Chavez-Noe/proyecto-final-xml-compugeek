(:Cosnultas XQuery:)
(: Mostrar el nombre de los productos :)
"1. Mostrar el nombre de los productos",
doc("C:\Users\noe_f\Dropbox\XML\Proyecto final XML\CompuGeekXSD.xml")/productos/producto/data(nombre) ,

(:2. Mostrar fechas de ventas :)
"Mostrar fechas de ventas",
for $fechas in doc("C:\Users\noe_f\Dropbox\XML\Proyecto final XML\CompuGeekXSD.xml")/productos/producto
return $fechas/data(fecha_venta),

(: Equipos de la marca Ghia :)
"3. Equipos de la marca Ghia",
for $equipos in doc("C:\Users\noe_f\Dropbox\XML\Proyecto final XML\CompuGeekXSD.xml")/productos/producto
where $equipos/marca = "Ghia"
return $equipos/nombre/data(),

(: Mostrar equipos con sistema operativo Android ordenado de manera acendente :)
"4. Mostrar equipos con sistema operativo Android ordenado de manera acendente",
for $equipos in doc("C:\Users\noe_f\Dropbox\XML\Proyecto final XML\CompuGeekXSD.xml")/productos/producto
where $equipos/sistema_operativo/@nombre = "Android"
order by $equipos
return $equipos/marca/data(),
"5. Listar el nombre de equipos con windows vendidos con 4GB de RAM ordenados de forma decendiente",
for $equipos in doc("C:\Users\noe_f\Dropbox\XML\Proyecto final XML\CompuGeekXSD.xml")/productos/producto
where $equipos/caracteristica/memoria_principal/@capacidad = "4GB" and $equipos/sistema_operativo/@nombre = "Windows"
order by $equipos
return $equipos/nombre/data()