<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <!-- Indicamos que la salida (output) será de tipo HTML -->
    <xsl:output method="html"/>
    
    <!-- Usamos XPath para comentar qué queremos parsear todo el XML -->
    <xsl:template match="/">
        <html>
            <body>
                <h1>Tienda CompuGeek</h1>
                <h5>Lista de productos</h5> <hr/>
                <table border="1px" align="top" >
                    <tr>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Precio</th>
                    </tr>
                    <xsl:for-each select="productos/producto">
                        <tr>
                            <td><xsl:value-of select="marca"/></td>
                            <td><xsl:value-of select="modelo"/></td>
                            <td><xsl:value-of select="precio"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>        
    </xsl:template>
</xsl:stylesheet>